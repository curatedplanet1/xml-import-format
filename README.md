# Curated Planet XML import format

This document briefly describes the general principles of XML import @ Curated Planet.
For more information, refer to the example XML files.

## Introduction

The XML format is intended to allow for general information exchange to and from the Curated Planet database.
The format is versioned, and the specification is updated when new features are added. Import processes data
according to the specification of the version declared.

Currently the following entities can be described by this XML format:

 - users
 - activities
 - itineraries
 - group tours

Itineraries are used to fill the trip catalog. Group tours are the actual trips for concrete users. Both can refer to activities
to describe the trip flow. Group tours should refer to users to list their participants.

### Import modes

Currently, the following two import modes are supported:

 - full
 - append

Full mode means that the imported XML file completely describes active itineraries and group tours. All itineraries and group tours
not listed in the file will be un-published and won't be displayed in the app.

Append mode means that the imported XML file describes only part of itineraries and/or group tours. Other itineraries and group tours,
that already exist in Curated Planet database, are not affected by the import.

Full mode is the default. Append mode must be explicitly switched on (see **Sources** section below).

### Import keys

Each company that wishes to import its data to Curated Planet, must obtain at least one import key. To do so, please contact
[support@curatedplanet.com](mailto:support@curatedplanet.com). The key uniquely identifies the information source for Curated Planet and should be used throughout
the XML file for each record (activity, itinerary, tour, user, etc).

Generally, the XML format allows multiple information sources in the same file. Each information source is identified by its key. Each record is processed or rejected independently according to the settings associated with its key.

The key is always specified using **source_id** attribute (see examples).

If all the records in the XML file belong to the same source, it is possible to declare the key only once in the **Sources** section. In this case,
**source_id** attribute can be omitted for all records. In all other cases it is mandatory.

Same company may be associated with more than one key. Keys can have restrictions on the types of records which are allowed to be imported (e.g., certain key can be limited to importing activities only). In the future, more settings may be added.

### Sources

Each XML file can adjust the import process settings independently for each information source. For that, a **sources** section must be declared as follows:

```xml
<sources>
    <source id="source_key">
        <external_sorting>true</external_sorting>
        <type>append</type>
        <policy>strict</policy>
    </source>
</sources>
```

Currently, 3 settings are available:

 - **external_sorting** - boolean (default: false). If true, the **itinerary.sequence** attribute is imported as is (value is assumed to be integer). This attribute defines the sorting order of the itineraries in the catalog. By default, order of the **itinerary** tags in the XML file is used. **itinerary.sequence** attribute is useful in two cases:
    - order of the **itinerary** tags should be overridden
    - for a partial import (type==append), it allows to position newly imported itineraries along with already existing ones. Note that in this case you are completely in control of how the itineraries are sorted. That is, you have to maintain the state (store the sorting order on Curated Planet) somewhere in your database.
 - **type** - enum: **full** (default) or **append**. See **Import modes** above. **full** mode will un-publish all existing itineraries and group tours which are not listed in the imported XML file. **append** mode won't do that.
 - **policy** - enum: **relaxed** (default) or **strict**. The latter mode (strict) will reject all the information in the XML file if there's at least one fatal error. The former mode (relaxed) will try to avoid rejection by making reasonable assumptions in case of certain errors or ambiguities. That is, relaxed mode will accept more erroneous data, but isn't guaranteed to interpret ambiguities as you might expect.

If the defaults look good, **sources** section can be omitted. Each record in the XML file is processed independently and must explicitly
define the key using the **source_id** attribute. However, if there's only one source in a file, and it is explicitly mentioned in the **sources** section, the **source_id** attribute can be omitted through the whole file. E.g., the following 2 examples are the same:

```xml
...
<activities>
    ...
    <activity source_id="1234">
        ...
    </activity>
    ...
</activities>
...
```

and

```xml
...
<activities>
    ...
    <activity>
        ...
    </activity>
    ...
</activities>
...
<sources>
    <source id="1234"/>
</sources>
...
```

### Import procedure

Currently, the only supported delivery method is HTTP. You should agree on a reasonable update schedule with
[support@curatedplanet.com](mailto:support@curatedplanet.com) and publish the updated XML in a well-known location. Curated Planet crawler
fetches the file according to the schedule and processes it. Each time it is done, a new import log record
is created. Each import is either successful or unsuccessful, and each import produces a log and a list of errors
(which may be empty). This information can be viewed from the tour operator dashboard. Email report can be sent to
a predefined list of email addresses.

### Validation

A RelaxNG and XSD schemas are available for each XML format version. Published files must be valid. Please note that
schemas verify only the file syntax. A technically valid file can still be an erroneous one, in which case errors will
be reported. Validity is necessary but not sufficient for the import to be successful.

Validation can be done using a number of tools available online (e.g., https://www.freeformatter.com/xml-validator-xsd.html)
or using [xmllint](http://xmlsoft.org/xmllint.html):

```bash
xmllint --schema cp-import.xsd --noout your-file.xml
```

or

```bash
xmllint --relaxng cp-import.rng --noout your-file.xml
```

XSD schema is generated from RelaxNG, so they are equivalent.

## XML syntax

Note: this section is work in progress

For now, please refer to the examples provided and to the comments in them. Syntax tries to be self-explanatory and relaxed. Nearly all
fields and attributes are optional.

## Version history

### 1.1.8
Allow to specify expiration date for media files and mark pre-trip items for offline usage; add examples of itinerary day description and image.

### 1.1.7
Add tour.save_as_template and tour.template attributes; many more fields are now optional

### 1.1.6
Add support for external links in activities, itineraries, tours and users

### 1.1.5
Allow specification of arbitrary metadata for custom processing

### 1.1.4
Unify day format in tours and itineraries

### 1.1.3
Allow time to be specified both in European (23:59) and US formats (11:59 PM)

### 1.1.2
Allow videos and documents as attachments

### 1.1.1
Strict and relaxed policies added for import

### 1.1.0
User model added, to allow users to be associated with tours

### 1.0.0
Initial release to support import of activities, itineraries, and tours

